﻿using AutoItX3Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

       



        string text;

        public MainWindow()
        {
            InitializeComponent();

            FileStream fileStream = new FileStream(@""+System.AppDomain.CurrentDomain.BaseDirectory+ "\\data.txt", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                text = streamReader.ReadToEnd();
            }

            List<string> sentence = new List<string>();
            sentence = text.Split('\n').ToList();
            for (int i = 0; i < sentence.Count; i++)
            {
                sentence[i] = ""+i+"."+sentence[i].Replace("\r", String.Empty)+" "+Environment.NewLine;

                input.Text +=sentence[i];

            }


    }
        #region ok
     
        #endregion





        int i =0;




        List<string> sentence = new List<string>();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            i = Int32.Parse(line.Text);
            sentence = text.Split('\n').ToList();
            //stt.Content = sentence.Count.ToString();
            sentence[i] = sentence[i].Replace("\r", String.Empty);
            output.Text = sentence[i];
            Clipboard.SetText(output.Text);

            //   MessageBox.Show(sentence.Count.ToString());

        }
        public void nextSentence()
        {
            i++;
            if (i < sentence.Count)
            {
                sentence[i] = sentence[i].Replace("\r", String.Empty);
                string k = "";
                if (sentence[i] != "")
                    k = sentence[i].Substring(sentence[i].Length - 1, 1);

                if (k != "." && k != "?")
                {
                    output.Text = "" + sentence[i] + ".";
                    if (output.Text != "  .")
                        Clipboard.SetText(output.Text);
                }
                else
                {
                    output.Text = sentence[i];
                    Clipboard.SetText(output.Text);
                }

            }
            if (i > sentence.Count)
            {
                MessageBox.Show("done");
            }

        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            nextSentence();
        }
       
            public void auto()
        {
         

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            i--;
            if (i < sentence.Count)
            {
                sentence[i] = sentence[i].Replace("\r", String.Empty);
                string k = sentence[i].Substring(sentence[i].Length - 1, 1);
                if (k != "." && k != "?")
                {
                    output.Text = "" + sentence[i] + ".";
                    Clipboard.SetText(output.Text);
                }
                else
                {
                    output.Text = sentence[i];
                    Clipboard.SetText(output.Text);
                }

            }
            if (i <0)
            {
                MessageBox.Show("done");
            }

        }

        private void behavior_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void oknhe(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                MessageBox.Show("dfdf");
        }




        #region hotkey
        [DllImport("User32.dll")]
        private static extern bool RegisterHotKey(
       [In] IntPtr hWnd,
       [In] int id,
       [In] uint fsModifiers,
       [In] uint vk);

        [DllImport("User32.dll")]
        private static extern bool UnregisterHotKey(
            [In] IntPtr hWnd,
            [In] int id);

        private HwndSource _source;
        private const int HOTKEY_ID = 9000;

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            var helper = new WindowInteropHelper(this);
            _source = HwndSource.FromHwnd(helper.Handle);
            _source.AddHook(HwndHook);
            RegisterHotKey();
        }

        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(HwndHook);
            _source = null;
            UnregisterHotKey();
            base.OnClosed(e);
        }

        private void RegisterHotKey()
        {
            var helper = new WindowInteropHelper(this);
            const uint VK_F10 = 0x0000;
            const uint MOD_CTRL = 0x0004;

       //     NoModifier = 0x0000,
        
        /// <summary>Specifies that the Accelerator key (ALT) is pressed with the key.
        /// </summary>
      //  Alt = 0x0001,
        /// <summary>Specifies that the Control key is pressed with the key.
        /// </summary>
     //   Ctrl = 0x0002,
        /// <summary>Specifies that the Shift key is pressed with the associated key.
        /// </summary>
      //  Shift = 0x0004,
        /// <summary>Specifies that the Window key is pressed with the associated key.
        /// </summary>
      //  Win = 0x0008


            if (!RegisterHotKey(helper.Handle, HOTKEY_ID, MOD_CTRL, VK_F10))
            {
                // handle error
            }
        }

        private void UnregisterHotKey()
        {
            var helper = new WindowInteropHelper(this);
            UnregisterHotKey(helper.Handle, HOTKEY_ID);
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            switch (msg)
            {
                case WM_HOTKEY:
                    switch (wParam.ToInt32())
                    {
                        case HOTKEY_ID:
                            OnHotKeyPressed();
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void OnHotKeyPressed()
        {
            nextSentence();
        }











        #endregion

        private void test_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }











    }
